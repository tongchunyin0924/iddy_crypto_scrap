var threadServices = require('./threadServices');
var postServices = require('./postServices');
var redditAccountServices = require('./redditAccountServices');

// BTC = Bitcoin
// XRP = Ripple
// ETH = ethereum
// ECH = Bitcoincash
// ADA = cardano
// XEM = nem

const subredditsList = [
    {
        reddit: 'CryptoCurrency',
        symbol: '',
        numberOfPost : 30
    },
    {
        reddit: 'Bitcoin',
        symbol: 'BTC',
        numberOfPost : 20
    },
    {
        reddit: 'Ripple',
        symbol: 'XRP',
        numberOfPost : 20
    },
    {
        reddit: 'ethereum',
        symbol: 'ETH',
        numberOfPost : 20
    },
    {
        reddit: 'Bitcoincash',
        symbol: 'ECH',
        numberOfPost : 10
    },
    {
        reddit: 'cardano',
        symbol: 'ADA',
        numberOfPost : 10
    },
    {
        reddit: 'nem',
        symbol: 'XEM',
        numberOfPost : 10
    }
];




var updatingIndex = 0;

exports.startUpdate = function () {
    updatingIndex = 0;
    startReddit(updatingIndex);
};

function startReddit() {
    updateReddit(subredditsList[updatingIndex]).then(function () {
        updatingIndex++;
        if (updatingIndex < subredditsList.length) {
            startReddit(updatingIndex);
        }
        else {
            //sleep and start

            console.log('************************************');
            console.log('Complete!!!!!!! Wait and restart');
            console.log('************************************');

            updatingIndex = 0;
            setTimeout(function () {
                //sleep 1min
                startReddit(updatingIndex);
            }, 60000);
        }
    });
}


function updateReddit(data) {

    var subreddit = data.reddit;
    var symbol = data.symbol;
    var numberOfPost = data.numberOfPost;

    console.log('************************************');
    console.log('Starting update reddit: ' + subreddit);
    console.log('************************************');

    const r = redditAccountServices.getRedditAccount();

    var promise = new Promise(function (resolve, reject) {

        var allSubmissions = [];
        var allSubmissionsID = [];
        var updateCount = 0;

        r.getSubreddit(subreddit).getNew({ limit: numberOfPost }).then(function (submissions) {
            allSubmissions = allSubmissions.concat(submissions);

            r.getSubreddit(subreddit).getTop({ limit: numberOfPost }).then(function (submissions) {
                allSubmissions = allSubmissions.concat(submissions);

                r.getSubreddit(subreddit).getRising({ limit: numberOfPost }).then(function (submissions) {
                    allSubmissions = allSubmissions.concat(submissions);

                    allSubmissions.forEach(submission => {
                        var submissionID = submission.id;
                        if (!allSubmissionsID.includes(submissionID)) {
                            allSubmissionsID.push(submissionID);
                        }
                    });

                    threadServices.startUpdate(allSubmissionsID).then(function () {
                        //update end one coin
                        resolve();
                    });

                }, function(error) {
                    console.log('Snoowrap Error!!!!!');
                    console.log(error);
                    resolve();
                });

            }, function(error) {
                console.log('Snoowrap Error!!!!!');
                console.log(error);
                resolve();
            });
        }, function(error) {
            console.log('Snoowrap Error!!!!!');
            console.log(error);
            resolve();
        });
    });

    return promise;
};


