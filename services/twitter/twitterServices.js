var Twitter = require('twitter');
var databaseServices = require('../databaseServices');
const uuidV1 = require('uuid/v1');

var db = databaseServices.getTwitterConnection();

const clientList = [
    new Twitter({
        consumer_key: 'b0D9jZWdhW0xNX6CXZ8TFw',
        consumer_secret: 'jDVl8ja1w5y0Hq5cuaJ5b0PD1KmUao8NIRyUVWfs5Hw',
        access_token_key: '15341844-YOb6Ei0JF7VqBZWnLrEtAMCMj0HijYypfFsAme8C1',
        access_token_secret: 'f9dBFsi4UQaJQHMoJyDWwBYsVbW8gBFFiCKPBOZ10U'
    }),
    new Twitter({
        consumer_key: 'cn6lPw2l0LFYTkK4cTdl4bCGB',
        consumer_secret: 'QFYJ7enS4OFfeM7BdVYenV3YQFbIDCb9oE4FMLN681lmLZvZib',
        access_token_key: '421004966-V6lL22dt7ATT2VWhzVxwvOGoIpmRO0wuncDIFdVj',
        access_token_secret: 'wuP6FY9bkAbRhAiEJFrXbVEhlVkq73yYUmfPOO5VUOZgz'
    }),
    new Twitter({
        consumer_key: '45Ipr9WJcTq2X0KXRXORSeb58',
        consumer_secret: '4WRn2q8qveAMB1LG3WoA7VPkMHBFyWlCZDjE1u4S48WCs9dZ1d',
        access_token_key: '820878393033883649-sVr816DrLqYinGZacKOazUGcmpCrGMw',
        access_token_secret: '2x9A56l0ogQbmMydZgBsVisHFmxm4GlUDnxoeChwoYY71'
    }),
];

const trackList = [
    {
        symbol: 'BTC',
        keyword: 'BTC BitCoin',
        client: clientList[0]
    },
    {
        symbol: 'ETH',
        keyword: 'ETH Ethereum Ether',
        client: clientList[0]
    },
    {
        symbol: 'XRP',
        keyword: 'XRP Ripple',
        client: clientList[1]
    },
    {
        symbol: 'BCH',
        keyword: 'BCH BitcoinCash',
        client: clientList[1]
    },
    {
        symbol: 'ADA',
        keyword: 'ADA Cardano',
        client: clientList[2]
    },
    {
        symbol: 'XEM',
        keyword: 'XEM NEM',
        client: clientList[2]
    },
];


var max_id = '';
var min_id = '';
var keyword = '';
var symbol = '';

exports.startStreaming = function () {

    trackList.forEach(function(info) {

        var symbol = info.symbol;
        var client = info.client

        selectThreadData(symbol).then(function () {

            console.log('[Twitter] init streaming ' + symbol);
    
            var stream = client.stream('statuses/filter', { track: symbol });
            stream.on('data', function (event) {
                // console.log('[Twitter]' && event && event.text);
    
                twitterInsertPost([event], symbol).then(function() 
                {
                    //insert end
                    // console.log('[Twitter] end ' + symbol + ': ' + event.text.length);
                });
            });
        });
    });

    
};


// exports.startGetHistory = function () {

//     selectThreadData('BTC').then(function () {
//         console.log('init finish BTC: ');

//         symbol = 'BTC';
//         keyword = 'BTC BitCoin';
//         recrusivelyGetTweetHistory(function () {
//             //end
//         });
//     })
// };

function recrusivelyGetTweetHistory(completion) {
    getTweetHistory().then(function () {
        recrusivelyGetTweetHistory(completion);

    }, function (error) {
        //end
        console.log('complete for getting tweet');
        completion();
    });
}

function getTweetHistory() {
    console.log('Twitter: Getting tweet history: ' + min_id);
    var promise = new Promise(function (resolve, reject) {
        client.get('search/tweets', { q: keyword, count: 100, max_id: min_id }, function (error, tweets, response) {
            if (error) {
                console.log(error);
                resolve();
            }
            else {
                var statuses = tweets.statuses;
                min_id = statuses[statuses.length - 1].id;

                console.log('newID: ' + min_id + ' number of data return: ' + statuses.length);

                if (max_id === '') {
                    //first time
                    max_id = statuses[0].id;
                }

                twitterInsertPost(tweets.statuses).then(function () {
                    console.log('Twitter insert post end');
                    resolve();
                });
                // statuses.forEach(status => {
                //     // console.log(status.created_at);
                // });

            }

        });
    });

    return promise;
}

//Thread
function selectThreadData(symbol) {

    var promise = new Promise(function (resolve, reject) {
        var source = 'TWEET';
        var forum = symbol;
        var thread_id = source + forum;

        var tid = thread_id;

        var sql = {
            source: source,
            source_forum: forum,
            source_tid: tid,
            source_created: '',
            subject: '',
            symbol: symbol,
            last_reply: ''
        };

        //update thread
        var updatequery = db.query('SELECT * from thread WHERE tid = ?', [tid], function (err, results, fields) {
            // console.log('changed ' + results.affectedRows + ' rows');

            if (err) {
                console.log(err);
                resolve();
            }
            else if (results.length == 1) {
                max_id = results[0].source_created;
                min_id = results[0].last_reply;

                resolve();
            }
            else {
                sql.id = uuidV1();
                sql.tid = tid;
                //need insert    
                var insertquery = db.query('INSERT INTO thread SET ?', sql, function (err, rows) {
                    if (err) {
                        console.log(err);
                        resolve();
                    }
                    else {
                        // console.log('Insert OK');
                        resolve();
                    }
                });
            }
        });
    });

    return promise;
}

function updateThreadLastReply(max_id, min_id) {
    var promise = new Promise(function (resolve, reject) {
        var source = 'TWEET';
        var forum = symbol;
        var thread_id = source + forum;

        var tid = thread_id;

        var sql = {
            source_created: max_id, //for start getting point
            last_reply: min_id
        };

        //update thread
        var updatequery = db.query('UPDATE thread SET ? WHERE tid = ?', [sql, tid], function (err, results, fields) {
            // console.log('changed ' + results.affectedRows + ' rows');

            if (err) {
                console.log(err);
                resolve(null);
            }
            else if (results.length == 1) {
                resolve(null);
            }
            else {
                resolve(null);
            }
        });
    });

    return promise;
}


//Post
function twitterInsertPost(data, symbol) {
    var promise = new Promise(function (resolve, reject) {

        var source = 'TWEET';
        var forum = symbol;
        var thread_id = '';

        var tid = source + forum + thread_id;
        var updateCount = 0;

        if (data.length === 0) {
            resolve();
            return;
        }

        // console.log('Twitter: Start Update Post: ' + tid + ' count: ' + data.comments.length);
        data.forEach(comment => {
            var postid = comment.id;
            var pid = tid + postid;

            var twitterDate = new Date(Date.parse(comment.created_at.replace(/( \+)/, ' UTC$1')));

            var sql = {
                id: uuidV1(),
                tid: tid,
                pid: pid,
                source_pid: pid,
                source_created: twitterDate,
                source_msg: comment.text,
                msg: comment.text,
                symbol: symbol
            };

            //update thread
            var insertquery = db.query('INSERT INTO post SET ?', sql, function (err, rows) {
                // console.log('finish insert db: ' + pid);
                updateCount++;
                if (err) {
                    //ignore
                    // console.log(err);
                }

                if (updateCount >= data.length) {
                    console.log('[Twitter] Insert Post finish for ' + symbol + ': ' + tid);
                    resolve();
                }
            });
        });

    });

    return promise;

};


