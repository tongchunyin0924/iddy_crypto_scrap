'use strict';
var express = require('express');
var router = express.Router();
var redditServices = require('../services/redditServices');
var twitterServices = require('../services/twitter/twitterServices');
var telegramServices = require('../services/telegram/telegramServices');

redditServices.startUpdate();
twitterServices.startStreaming();
// telegramServices.startPolling();

/* GET home page. */
router.get('/', function (req, res, next) {

});


// https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets

module.exports = router;
