var mysql = require("mysql");

var connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'alfa1668',
    database: 'crypto'
});

var twitterConnection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'alfa1668',
    database: 'crypto'
});

function connect() {
    connection.connect(function (err) {
        if (err) {
            console.log('connecting error');
            return;
        }
        console.log('connecting success');
    });

    twitterConnection.connect(function (err) {
        if (err) {
            console.log('connecting error');
            return;
        }
        console.log('connecting success');
    });
};

connect();

exports.getConnection = function() {
    if (connection)
    {
        return connection;
    }
    else {
        console.log('get connection error');
        return null;
    }
}


exports.getTwitterConnection = function() {
    if (twitterConnection)
    {
        return twitterConnection;
    }
    else {
        console.log('get connection error');
        return null;
    }
}
