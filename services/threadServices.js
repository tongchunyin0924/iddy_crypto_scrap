var databaseServices = require('./databaseServices');
var postServices = require('./postServices');
var redditAccountServices = require('./redditAccountServices');

const uuidV1 = require('uuid/v1');

var db = databaseServices.getConnection();

var updatingIndex = 0;

exports.startUpdate = function (allSubmissionsID, symbol) 
{
    var promise = new Promise(function (resolve, reject) 
    {
        updatingIndex = 0;
        startThread(allSubmissionsID, symbol, updatingIndex, function() {
            resolve();
        });
    });
    return promise;
};

function startThread(allSubmissionsID, symbol, index, completion) 
{
    const r = redditAccountServices.getRedditAccount();
    
    if(index < allSubmissionsID.length -1 )
    {
        var submissionID = allSubmissionsID[index];
        console.log('ready to get submission: ' + submissionID);   
        r.getSubmission(submissionID).expandReplies({ limit: Infinity, depth: Infinity }).then(function (reply) 
        {
            console.log('Get submission finish: ' + submissionID);   
            updateThread(reply, index).then(function () {
                
                postServices.updatePost(reply, symbol).then(function () {
                    //update post success
                    updatingIndex++;
                    // setTimeout(function()
                    // {
                        //sleep 1s
                        startThread(allSubmissionsID, symbol, updatingIndex, completion);

                    // }, 3000);
                });
            });
        }, 
        function (error) 
        {
            console.log(error);
            
            redditAccountServices.switchAccount();
            setTimeout(function()
            {
                //sleep 1min
                startThread(allSubmissionsID, symbol, updatingIndex, completion);

            }, 30000);                    
        });
    }
    else {
        //end
        completion();
    }
};

function updateThread(data, symbol) {

    var promise = new Promise(function (resolve, reject) {

        var last_reply = '';
        if (data.comments.length > 0) {
            var last_comment = data.comments[data.comments.length - 1];
            last_reply = last_comment.id;
        }

        // console.log('last reply: ' + data.comments.length);
        // console.log('last reply: ' + JSON.stringify(data.comments) );

        var source = 'REDIT';
        var forum = data.subreddit_name_prefixed;
        var thread_id = data.id;

        var tid = source + forum + thread_id;

        var title = data.title;
        if (title.length > 200)
        {
            console.log('Warning title too long!');
            title = title.substring(0, 200);
        }

        var sql = {
            source: source,
            source_forum: forum,
            source_tid: data.id,
            source_created: data.created_utc,
            subject: title,
            symbol: symbol,
            last_reply: last_reply
        };

        //update thread
        var updatequery = db.query('UPDATE thread SET ? WHERE tid = ?', [sql, tid], function (err, results, fields) {
            // console.log('changed ' + results.affectedRows + ' rows');

            if (err) {
                console.log(err);
                resolve();
            }
            else if (results.affectedRows == 1) {
                // console.log('Update OK');
                resolve();
            }
            else {
                sql.id = uuidV1();
                sql.tid = tid;
                //need insert    
                var insertquery = db.query('INSERT INTO thread SET ?', sql, function (err, rows) {
                    if (err) {
                        console.log(err);
                        resolve();
                    }
                    else {
                        // console.log('Insert OK');
                        resolve();
                    }
                });
            }
        });
    });

    return promise;
};



