var databaseServices = require('./databaseServices');
const uuidV1 = require('uuid/v1');

var db = databaseServices.getConnection();

exports.updatePost = function (data, symbol) {

    var promise = new Promise(function (resolve, reject) {

        var source = 'REDIT';
        var forum = data.subreddit_name_prefixed;
        var thread_id = data.id;

        var tid = source + forum + thread_id;
        var updateCount = 0;

        if(data.comments.length === 0)
        {
            resolve();
            return;
        }

        console.log('Start Update Post: ' + tid + ' count: ' + data.comments.length);
        // console.log('ready for update post: ' + data.comments.length);
        data.comments.forEach(comment => {
            var postid = comment.id;
            var pid = tid + postid;

            var sql = {
                source_pid: pid,
                source_created: comment.created_utc,
                source_msg: comment.body,
                msg: comment.body,
                symbol: symbol
            };

            //update thread
            // console.log('ready update db: ' + pid);
            var updatequery = db.query('UPDATE post SET ? WHERE pid = ?', [sql, pid], function (err, results, fields) {
                // console.log('changed ' + results.affectedRows + ' rows');

                // console.log('finish update db: ' + pid);

                if (err) {
                    updateCount++;
                    console.log(err);
                }
                else if (results.affectedRows == 1) {
                    updateCount++;
                }
                else {
                    sql.id = uuidV1();
                    sql.tid = tid;
                    sql.pid = pid;

                    //need insert    
                    // console.log('ready insert db: ' + pid);
                    var insertquery = db.query('INSERT INTO post SET ?', sql, function (err, rows) {
                        // console.log('finish insert db: ' + pid);
                        updateCount++;
                        if (err) {
                            console.log(err);
                        }

                        if (updateCount >= data.comments.length) {
                            console.log('Insert Post finish for Post: ' + tid);
                            resolve();
                        }
                    });
                }

                if (updateCount >= data.comments.length) {
                    console.log('Update Post finish for Post: ' + tid);
                    resolve();
                }
            });
        });

    });

    return promise;

};



